package ru.csu.springsecurity.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.csu.springsecurity.dto.request.LoginRequest;
import ru.csu.springsecurity.dto.response.JwtResponse;
import ru.csu.springsecurity.service.impl.SecurityService;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final SecurityService securityService;

    @PostMapping("/login")
    @ResponseBody
    public JwtResponse signIn(@RequestBody LoginRequest loginRequest) {
        return securityService.login(loginRequest);
    }

}
